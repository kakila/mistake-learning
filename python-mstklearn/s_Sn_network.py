# Copyright (C) 2020 Juan Pablo Carbajal
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

import itertools as itt

from math import factorial
import numpy as np
import networkx as nx

import matplotlib.pyplot as plt

if 'n' not in locals():
    n = 4

nbijections = factorial(n)
nodes_in = range(1, n+1)
nodes_out = range(n+1, 2*n+1)


nr = int(np.sqrt(nbijections))
nc = int(nbijections / nr + np.remainder(nbijections, nr))
fig, axs = plt.subplots(ncols=nc, nrows=nr)
plt.subplots_adjust(left=0.03, right=0.97, bottom=0.03, top=0.95, 
                    hspace=0.5, wspace=0.5)

axs = axs.ravel()
colormap = plt.get_cmap('Set1')
colors = np.asarray(list(nodes_in) * 2) - 1
pos = {n:np.asarray(p) for n, p in zip(
                            list(nodes_in)+list(nodes_out), 
                            itt.product([-1,1],np.linspace(-1+1/n, 1-1/n, n)[::-1])
                         )}
for i, eset in enumerate(itt.permutations(nodes_out)):
    G = nx.Graph(zip(nodes_in, eset))
    nx.draw(G,
            pos,
            node_size=20,
            node_color=colors[np.asarray(G.nodes)-1],
            vmin=0, vmax=n-1, cmap=colormap,
            with_labels=False,
            ax=axs[i],
           )
    axs[i].title.set_text(''.join(f'{x-n}' for x in eset))
    axs[i].title.set_fontsize(6)

plt.show()
