Title: A memristive Chialvo-Bak machine
Date: 2020-08-06 07:40
Modified: 2020-08-21 23:53
Tags: idea, theory
Category: Concepts
Slug: mem-chialvo-bak
Authors: JuanPi Carbajal
Summary: a memristive implementation of the Chialvo-Bak learning machine


A Chialvo-Bak learning [@Bak2001] machine (CBM) is a weighted directed graph in which signals entering the input nodes
propagate through the edges with the largest/smallest weights ([winner-takes-all or WTA dynamics]({filename}/wtadynamics.md)).
On each node the propagation follows the direction of outgoing edges, hence between two adjacent nodes the direction
of propagation is prescribed by the topology (it does not depend on the signal, not on the choice of input/output nodes).
This last property will be an issue for the memristive implementation of the machine that we propose here, more on this later.

In the original CBM, the propagated signal is binary, indicating whether a node is active or not. 
Therefore the networks are used to implement relations between two sets of integers.
That is, by labeling the input nodes with $N_\text{in}$ integers and the output nodes with $N_\text{out}$ integers, the
network can learn associations between the $N_\text{in}$ and $N_\text{out}$ integers (see the [Function between finite 
sets]({filename}/functionfinitesets.md) post for formal details).

The structure of the memristive version of the machine is shown in the plot below
 
![the structure of a memristive Chialvo-Bak machine]({static}/images/memchialvobak.svg)

The memristive machine is an electric network (directed multi-graph). 
Each edge in the network is either a resistor (fixed resistance) or a memristor (resistance depends on the current).
The weight of the edges in the Chialvo-Bak machine are implemented with the resistances in the electric network.  

Signals are introduced to the network using controlled voltage sources connected between the input nodes and the ground 
(GND) node[ref]Actually, it is the edges of the electric network that are associated with the nodes of the Chialvo-Bak machine.
That is, we have input/output *edges* instead of nodes.
However this difference is technical, relevant for software implementations and formalism.[/ref].
These sources can be in *signal*, *teaching*, or *open* mode.
The signal mode is used to propagate a signal through the network and evaluate the output.
The teaching mode is used to adapt the weights in the network.
The open mode is also used during learning, as explained in later sections.

The output nodes are connected to the GND node via *cables* (ideal conductors), and we are interested in the current flowing 
through the edge between output nodes and the GND node.
This is depicted with the amperimeter symbol in the figure.

## Propagation
To propagate a signal through the network and evaluate the output, we set only one input into signal model.
We record the current through the outputs during the activation of the input.
From the recorded signals we apply a criteria to choose only one output as active.

The criterion to choose the active output is arbitrary, herein we use winner-takes-all: _the output with the largest 
mean squared current is designated as the active output_:

$$
\text{active output} = \mathop{\text{arg max}}_{n\, \in\, \text{Out}} \frac{1}{T}\int_0^T I_n(t)^2 \text{d}t 
$$   

where $T$ is the duration of the input signal, and $I_n$ is the current through the output $n$.

## Learning

As in the original work, learning is triggered only by mistakes.
The mistakes of the network are determined by comparison with a _task table_, for example, a minimalistic example of a 
task table would be

<style>
table{
    border-collapse: collapse;
    border-spacing: 0;
    width: 150px;
    margin-left: 0;
    margin-right: auto;
}
th{
    border-bottom:2px solid #000000;
}
td{
    border-bottom:1px solid #000000;
}
</style>
|**input** | **output** |
|----------|------------|
| $1$      | $2$        |

In this minimal example we have a single input (labeled $1$) and two outputs (labeled $2$ and $3$), and the task is 
requesting that when the input is active, only the output labeled $2$ is active.

In the figure below we show the steps that would be triggered if the network would not show the desired behavior: 

![example of learning algorithm]({static}/images/memchialvobak_learning.svg)

In the left panel we see the initial configuration of the network, which leads to an error: the resistance in edge $(1, 3)$
is smaller than the resistance in edge $(1, 2)$, hence the current through the former is larger.
This is shown in the middle panel, where the input is set to signal model and the active output marked with red.
The right panel shows the teaching configuration, that can be setup following these steps:

1. Set to open all inputs and outputs that were not active in the test.
That is, all but the active input dictated by the task table and the observed active output (which differs from the output in the table, otherwise we wouldn't be teaching) are set to open.
   Here output $2$ is set to open.

2. Set the input indicated by the task table to teaching mode

To achieve the desired effect (show in the right panel) the teaching mode sets a constant negative voltage for a 
predetermined time interval. 
The resistance of the $(1, 3)$ edge grows after teaching, and eventually becomes bigger than the resistance in the other 
edge.
At this point the network will not make any more mistakes (activation of $1$ will lead to activation of $2$), and the 
teaching will not be triggered.

If one of the memristors were to have an inverted polarity, the teaching will never achieve the desired effect. 
We shown this in the figure below:

![failure of learning algorithm]({static}/images/memchialvobak_notlearning.svg)

Hence for this teaching method, and for networks with feedforward topologies like this one, we need to warrant that all 
memristors have the same polarity.

## Next

In a follow up post we will analyze the behavior of a network like the one shown below (a bipartite graph):

![bipartite network topology]({static}/images/memchialvobak_bipartite.svg)

 
