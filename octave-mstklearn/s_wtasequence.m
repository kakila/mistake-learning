# Copyright (C) 2020 Juan Pablo Carbajal
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

## We look at the long term evolution of the WTA dynamics
#
N = 4;

function [ids w] = wta_update(ids, w, dw)
  # punish
  w(1)-=dw;
  # sort
  [w, ord] =sort (w, 'descend');
  # normalize
  w /= sum (abs (w));
  # returnd updated ids
  ids = ids(ord);
endfunction

w = sort(rand(N,1) + 0.1, 'descend');
ids = (1:4).';

Nwarm = 500;
Nsave = 1000;
w_  = zeros (N, Nsave);
id_ = zeros (N, Nsave);
#dw = max (abs (dif f(w)));
#dw = min (abs (diff (w)));
dw = 0.261;
for i=1:(Nwarm + Nsave)
  if i <= Nwarm
    # warm up to remove transient
    [ids w] = wta_update(ids, w, dw);
  else
    j = i - Nwarm;
    if j == 1
      id_(:,j) = ids;
      w_(:,j) = w;
    else
      [id_(:,j) w_(:,j)] = wta_update(id_(:,j-1), w_(:,j-1), dw);
    endif
  endif
endfor

# Plot
t = (1:Nsave)+Nwarm;
figure (1)
clf
subplot (3,1,1)
h_ = plot (t, w_.', '-');
ylabel ('weights')
axis tight

title (sprintf('weight step: %.3f', dw))

subplot(3,1,2)
colors = cell2mat(get(h_, 'color')(id_(1,:)));
scatter((1:Nsave)+Nwarm, id_(1,:), 25, colors, 'filled');
hold on
plot(t, id_(1,:), '-', 'linewidth', 0.5);
hold off
set(gca, 'ytick', 1:4);
xlabel ('iteration')
ylabel('winner ID')
axis tight

subplot(3,1,3)
hist (id_(1,:).', 1:4, 'facecolor', [0.8 0.8 0.8])
set(gca, 'xtick', 1:4)
xlabel('winner ID')
ylabel('frequency')
axis tight


## Sequence characterization
#

Ndw = 50;
dw = linspace(1e-3, 0.4, Ndw);
wta_period = cell(Ndw, 1);
for k=1:Ndw
  w_  = zeros (N, Nsave);
  id_ = zeros (N, Nsave);
  w   = linspace (1, 0.1, N);
  ids = (1:4).';

  for i=1:(Nwarm + Nsave)
    if i <= Nwarm
      # warm up to remove transient
      [ids w] = wta_update(ids, w, dw(k));
    else
      j = i - Nwarm;
      if j == 1
        id_(:,j) = ids;
        w_(:,j) = w;
      else
        [id_(:,j) w_(:,j)] = wta_update(id_(:,j-1), w_(:,j-1), dw(k));
      endif
    endif
  endfor
  wta_period{k} = unique(diff (find (id_(1,:) == 1)));
endfor # over steps

figure (2)
clf
hold on
for i=1:Ndw
  plot(dw(i), wta_period{i}, 'ok')
endfor
hold off
xlabel ('weight step')
ylabel ('period of id=1')

