# Copyright (C) 2020 Juan Pablo Carbajal
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

function [net out mistake niter] = train_WTAint(net, task, outnodes, maxsteps=1, strategy='rotate')

  # Select network update function
  deg = sum (isnodeid(net), 2); # out degree
  switch strategy
    case 'rotate'
      if any(!isnodeid(net(:)))
        updatefunc = @(x)na_shift(x, deg);
      else
        updatefunc = @(x) shift(x, -1, 2);
      endif
    case 'randperm'
      updatefunc = @(x)na_randperm(x, deg);
    otherwise
      error ('Octave:invalid-input-arg', sprintf('Unkown strategy %s', strategy))
  endswitch

  for i=1:maxsteps
    # WTA propagate inputs and evaluate
    [mistake out] = evaluate(net, task, outnodes);
    if all(!mistake)
      niter = i - 1;
      return
    endif
    net(mistake, :) = updatefunc(net(mistake, :));
  endfor
  niter = i;

endfunction

function [mistake out] = evaluate(net, tsk, outn)
  out = propagate_WTAint (net, tsk(:,1), outn);
  mistake = (out != tsk(:,2));
endfunction

function x = na_shift(x, d)
  for i=1:size(x, 1)
    x(i, 1:d(i)) = shift (x(i, 1:d(i)), -1);
  endfor
endfunction

function x = na_randperm(x, d)
  for i=1:size(x, 1)
    x(i, 1:d(i)) = x(i, 1:d(i))(randperm (d(i)));
  endfor
endfunction
