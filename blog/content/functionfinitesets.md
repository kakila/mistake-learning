Title: Function between finite sets
Date: 2020-06-14 22:24
Modified: 2020-06-14 22:24
Tags: idea, theory
Category: Concepts
Slug: discrete-functions
Authors: JuanPi Carbajal
Summary: We count the number of functions between two finite sets, and look at the structure of the subset of injective functions

A surjective function between two finite sets 

$$
f : \text{In} \rightarrow \text{Out}
$$

If $\vert \text{In} \vert = N_\text{in}$ and $\vert \text{Out} \vert = N_\text{out}$,
the total number of functions is (the size of the set of all functions between the two sets) is $N_\text{out}^{N_\text{in}}$

For sets with $N_\text{in} = N_\text{out} = n$ elements there are $n!$ bijective functions.
The complementary set of functions that are not bijective is then $n^n - n!$.
The ratio of bijective functions to the total is

$$
\frac{n!}{n^n} 
$$

Using [Striling's approximation](https://en.wikipedia.org/wiki/Stirling%27s_approximation) we get

$$
\sqrt{2 \pi n}e^{-n} \leq \frac{n!}{n^n} \leq e\sqrt{n}e^{-n}
$$

which goes to zero with exponential convergence. That is, for large $n$ the set of bijective functions is relatively small.

The functions can be represented as sequences. A few examples for $\text{In} = \text{Out} = \lbrace 1,2,3,4\rbrace$ are

- $f(x) = 1, \; \forall x \in \text{In}$ : $1,1,1,1$ or just 1111 for short.

- Any bijective function is a permutation, e.g. $(2\; 4\; 3\; 1)$ or 2431 for short.

These functions can be labeled with a $N_\text{in}$-ary number of $N_\text{out}$ digits, as seen in the examples above.

The second examples is nothing but the **symmetric group** $S_4$ of the set.
There are $4! = 24$ of such functions. The group is shown below (Cayley graph), with arrow colors denoting a particular permutation.

<a title="original: Fool (talk)
derivative: Watchduck (a.k.a. Tilman Piesk) / Public domain" href="https://commons.wikimedia.org/wiki/File:Symmetric_group_4;_Cayley_graph_4,9.svg"><img width="512" alt="Symmetric group 4; Cayley graph 4,9" src="https://upload.wikimedia.org/wikipedia/commons/thumb/b/b5/Symmetric_group_4%3B_Cayley_graph_4%2C9.svg/512px-Symmetric_group_4%3B_Cayley_graph_4%2C9.svg.png"></a>

Another way of looking at this group is using networks ([source code](https://gitlab.com/kakila/mistake-learning/-/blob/master/python-mstklearn/s_Sn_network.py))

<img alt="S4 as networks" src="{static}/images/S4AsNetworks.png" width="600" >

These networks can be concatenated in any order.
Considering the relation "$a$ is related with $b$ if we can go from $a$ to $b$ by concatenation", this group forms an equivalence class and can also be associated with the complete graph in $4$ elements.

In the context of our work, each of the networks in the figure above is the *winner-takes-all* (WTA) solution for one of the bijective functions.

## Functions as tasks
Since the functions can be represented as tuples, each of the tuples in the definition of the function is a potential input-output data point.
For example, the function 4132 is defined by the data $\lbrace (1,4), (2,1), (3,3), (4,2)\rbrace$ (the edges in the WTA solution).

### Learning rate for bijective functions
If we assume that we will sample without replacement data points from a bijective function, how many samples we need to identify the function?
The first sample eliminates $n$ functions (all networks without this edge are ruled out). Leaving $n! / n = (n-1)!$ functions
The second sample eliminates $n-1$ functions, leaving $(n-2)!$, and so on.
After the $i$th sample we have $(n - i)!$
After $n-1$ unique samples, $(n - n + 1)! = 1$, the function is completely identified.

Also, after $n-1$ unique samples there is a total of $n$ functions, from which one is bijective.

To be continued ...
