\documentclass[12pt,a4paper,english,fleqn]{article}
\usepackage{graphicx}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage[colorlinks=true]{hyperref}
\usepackage[numbers, sort&compress]{natbib}
\usepackage{authblk}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{textcomp}
\usepackage{lmodern}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                        %
%  BEGINNING OF TEXT              %  VERSION November 13, 2020
%                        %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}
\title{Memristor networks learn by mistakes}

%%%% old appolb class %%%%%
%\author{Juan Pablo Carbajal{$^\S$}, Daniel A.
%Martin {$^\dag~^\ddag$} \& Dante R.
%Chialvo{$^\dag ~^\ddag$}
%\address{{$^\S$}}
%\address{{$^\dag$}Center for Complex Systems and Brain Sciences (CEMSC${^3}$) \& {$^\ddag$}Instituto de Ciencias F\'isicas (ICIFI-Conicet), Escuela de Ciencia y Tecnolog\'ia, Universidad Nacional de Gral.
%San Mart\'in, Campus Miguelete, 25 de Mayo y Francia (1650), San Mart\'in, Buenos Aires, Argentina.}
%}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\author[$\dagger$]{Juan Pablo Carbajal}
\author[$\star,\ddag$]{Daniel A. Martin}
\author[$\star,\ddag$]{Dante R. Chialvo}

\affil[$\dagger$]{Institute for Energy Technology, University of Applied Sciences Rapperswil, Switzerland}
\affil[$\star$]{Center for Complex Systems and Brain Sciences (CEMSC${^3}$)}
\affil[$\ddag$]{Instituto de Ciencias Físicas (ICIFI-Conicet), Escuela de Ciencia y Tecnología, Universidad Nacional de Gral. San Martín, Campus Miguelete, 25 de Mayo y Francia (1650), San Martín, Buenos Aires, Argentina}

\maketitle


\begin{abstract}
Recent results in adaptive matter revived the interest in the implementation of novel devices able to perform brain-like operations.
Here we introduce a training algorithm for a memristor network which is inspired in previous work on biological learning.
Robust results are obtained from computer simulations of a network of voltage controlled memristive devices.
Its implementation in hardware is straightforward, being scalable and requiring very little peripheral computation overhead.
\end{abstract}

\section{Introduction}
 
In the last decade we have witnessed an explosion in the interest on neuro-morphing, i.e., novel adaptive devices inspired in brain principles.
Many of the current efforts focus on the replication of the dynamics of a single neuron, using a diversity of technologies including magnetic, optics, atomic switches, etc~\cite{Review1}.
While the emulation of a single neuron seems straightforward for most of these technologies, we still lack learning algorithms to train large interconnected neuron-like elements, without resorting to \emph {peripheral computation overheads.}

In thinking about this issue, it is soon realised that {\it in vivo} biological learning exhibits important features which are not presently considered in neuromorphing implementations.
The most relevant one, in the context of these notes, is the fact that the only information a biological neuron has at its disposal to modify its synapsis is either {\it global} or {\it local}.
In other words, in real brains, there is no \emph {peripheral computation overheads}, the strength of the synaptic weight between any two given neurons, is a function of the activity of its immediate neighbours and/or (trough some so-called neuro-modulators) some global state of the brain (or partial region of), resulting from success or frustration in achieving some goal (or being happy, angry, excited, sleepy, etc).
These observations leaded to the proposal~\cite{bak,bak1,bak2,joe1,joe2,brigman} of a simple neural network model able to learn simple input/output associations.

The present article instantiates a translation of the work of Refs~\cite{bak,bak1} into the realm of memristive networks.
The main objective is to design a device working on the principles described there, able to be fully implemented in hardware, not requiring any external sophisticated processing unit.

This article is organized as follows.
The next section is dedicated to review previous work~\cite{bak,bak1,bak2,joe1,joe2,brigman} describing a self-organized process by which biological learning may proceed.
Such work is the inspiration for the algorithm proposed here to train a network of memristors ~\cite{chua,ReviewCarvelli} which is described in Section 3.
The main results are contained in Section 4 where the simulation results obtained from a three layer feedforward network are presented.

The paper closes with a short list of expected hurdles to surpass and other possible similar implementations.
Numerical details and model parameters are described on the Appendix, together with some miscellaneous observations.

\section{A toy model of biological learning}

Two decades ago, Chialvo \& Bak~\cite{bak1} introduced a unconventional model of learning which emphasized self-organization.
 In that work, they re-examined the commonly held view that learning and memory necessarily require potentiation of synapses.
Instead, they suggested that, for a naive neuronal network, the process of learning involves making more mistakes than successful choices, thus the process of adapting the synapses would have more opportunities to punish the mistakes than to positively reinforce the successes.
Consequently, their learning strategy used two steps: the first involves extremal dynamics to determine the propagation of the activity through the nodes and the second using synaptic depression to decrease the weights involved in the undesired (i.e., mistaken) outputs.
The first step implies to select only the strongest synapses for propagating the activity.
The second step assumes that  active synaptic connections are temporarily tagged and subsequently depressed if the resulting output turned out to be unsuccessful.
Thus, all the synaptic adaptation leading to learning is driven only by the mistakes.

The model considered an arbitrary network of nodes connected by weights.
Although almost any network topology can be used, for the sake of description let discuss the simplest version of a three feedforward layered network (see Fig.~\ref{fig1}A).
To describe the working principle, let suppose that we wish to train the network to associate an input vector with a given output vector.
Thus the learning algorithm needs to modify the network's weights in such a way that a string of binary digits in the input layer is mapped with any desired string at the output layer.
 
The entire dynamical process goes as follows:

\begin{enumerate} 
\item Activate an input neuron $i$ chosen randomly from the set established by the task to learn.
\item Activate the neuron $j_m$ in the middle layer connected with the input neuron $i$ with the {\it largest $w(j,i)$}.
\item Activate the output neuron $k_m$ with the {\it largest $w(k,j_m)$}.
\item If the output $k$ happens to be the desired one, nothing is done.
\item \label{deltastep} Otherwise, that is if the output is not correct, $w(k_m,j_m)$ and $w(j_m,i)$ are both reduced (depressed) by an amount $\delta$.
\item Go back to 1.
Another input neuron from the task set is randomly chosen and the process is repeated.
\end{enumerate} 

That is all.
The process involves a principle of extremal dynamics (here simplified by choosing the strongest weights) followed --in case of incorrect output-- by negative feedback (i.e., the step 5 of adaptation).
The only parameter of the model is $\delta$, but, at least in the numerical simulations, it is not crucial at all, because its only role is to prevent the same path from the input to the undesired output to be selected more than once.
Numerical explorations with this simple model showed that step \ref{deltastep} above can be modified in many different ways (including choosing random values) without serious consequences, as long as it makes less probable the persistence of ``wrong paths''.

Notice that this model omits to consider the neuronal dynamics, i.e., the spikes of neuronal activity that characterise real brains.
Spikes only rol is to propagate the activity across the network, and because that propagation occurs most often (statistically speaking) across the strongest synapsis one can omit modelling the spikes and selecting the active nodes as it is done in the steps 2 \& 3 of the algorithm.

 %%%%%% Figure 1 %%%%%%%%%%%
\begin{figure} [ht!] 
\centerline{
\includegraphics [width = .85 \textwidth] {Fig1New.pdf} }
\caption{\label{fig1} Panel A: An example of a three-layer network with 3 input nodes, 4 intermediary nodes and 3 output node.
Each input node has a ``synaptic'' connection to every intermediary node, and each intermediary node has a similar connection to every output node.
 Panel B: A typical run to learn the five simple input-output patterns of the right plots.
As noted in the upper traces, the error eventually reaches zero, after which learning of a new pattern is attempted.
Re-drawn from~\cite{bak1}.
} 
\end{figure}
 %%%%%% Figure 1 %%%%%%%%%%%%%%

Fig.~\ref{fig1}B reproduce the results of a typical simulation in which few simple input-output maps (indicated on the right plots) are successively learned by the model (see details in~\cite{bak1}).
The error, computed as the squared distance between the actual output and the desired one, is seen to fluctuate until eventually vanishes.
Interference between maps is expected for relatively small system size, since the same path can be chosen by chance for two different input-output maps.
As was discussed earlier in Ref.~\cite{bak1,bak2} a system trained under these premises is robust with respect to noise; in the sense that depression of synaptic weights will self-adjust proceeding to correct the errors, until eventually achieving the desired input outputs.
Another interesting property of this set-up is that the learning time goes down with the size of the middle layer, a fact that is easily understood since the learning process implies to find an keep the strongest paths between the input and the desired nodes in the output layer.
This and other scaling relations can be found in Refs.~\cite{bak1,bak2,joe1,joe2,brigman}.
 
\section{Implementation on a memristor network }
Now we turn to discuss how to implement the toy model just described on a network of memristors\footnote{The computer code to reproduce these results can be downloaded from \url{https://github.com/DanielAlejandroMartin/Memristor121} (tag v1.0). A complementary library written in python is also being developed at \url{https://gitlab.com/kakila/memnet}}.
Memristive devices are a family of two-terminal devices whose resistance evolves according to the bias and currents they experience~\cite{strukov}.
In analogy to long term potentiation and depression taking place in neuronal synapses, memristor resistances can be increased or decreased through the application of relatively high voltage differences or currents.
%An example of increasing the resistance of two memristors in series is shown in Fig.~\ref{Fig2}.
%Memristors sketched in Fig.~\ref{Fig2}A are subject to a relatively large voltage difference $V$, given by a triangular, positively biased voltage, as seen in Fig.~\ref{Fig2}B.
%This voltage is large enough, such that produces a changes in the device' internal state (represented here by it's resistance) over time.
%Consequently, the resulting current, shown in Fig.~\ref{Fig2}C, tends to decrease over time, resulting on the hysteretic I-V curve shown in Fig.~\ref{Fig2}D.


%%%%%% Figure 2 %%%%%%%%%%%%%%
\begin{figure} [ht!] 
\centerline{
\includegraphics [width = .75\linewidth] {Fig2New.pdf} }
\caption{Behavior of a voltage controlled memristor connected as shown in panel A.
Typical changes in the properties of the memristor as a function of a time dependent voltage $V$ (panel B). 
The resulting current $I$ and the instantaneous resistance are shown in panels C and D, respectively.
The same data is presented in panel as an I-V curve (panel E).
Notice that relatively small voltage excursions (i.e., the upwards triangular sweeps) do not change the device resistance, while relatively large voltage excursions does it, resulting in the typical hysteresis loop.
Results in panels B-E have been colored to ease the interpretation.
Memristor parameters are detailed in the Appendix.}
\label{Fig2} 
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


In Figure \ref{Fig2}, we show an example of the typical changes exhibited by voltage controlled memristors when subjected to a voltage sources of different amplitudes.
The circuit is depicted in Fig.~\ref{Fig2}A, while the voltage and current across the device and the memristor resistance are shown in Figs.~\ref{Fig2}B to D.
The voltage source applies three triangular shaped low \emph{positive} voltage pulses, which do not change memristor's resistance, followed by a high, \emph{negative}, voltage excursion, which results on an increase of the memristor's resistance.
The final triangular low voltage pulse shows that the resulting resistance increase is permanent.
This property will be used here to modify the network input/output paths, as explained in the following paragraphs.

The results presented here correspond to numerical simulations of a three layer network of memristors, with $N_\text{in}$ input nodes, $N_\text{bulk}$ bulk nodes, and $N_\text{out}$ output nodes.
All memristors used are Voltage Controlled Memristors (VCM), which have a threshold $V_\updownarrow$.
The training algorithm uses an ammeter and a voltage source with two possible values: $V_\text{read}$ (read voltage) and $V_\text{write}$ (punishment or correction voltage).
The network and control resources are as sketched in Fig.~\ref{fig2018}.
For a detailed description of the memristor parameters please see the Appendix.
 
%%%%%% Figure 3 %%%%%%%%%%%%%%
\begin{figure} [ht!] 
\centerline{
\includegraphics [width = .5\linewidth] {Fig3New.pdf} }
\caption{Sketch of the learning algorithm for a network with $N_\text{in}=2$, $N_\text{bulk}=4$ and $N_\text{out}=2$.
In the Reading step (Panel A) a relatively small $V_\text{read}$ voltage is applied and the current at each output node is measured by the ammeter.
The $o_n$ with the largest current is defined as the output.
 If that output is no desired, in the Correction step (Panel B), a relatively large $V_\text{write}$ voltage of opposite polarity is applied to alter the resistance of the memristor path.
The cycle is repeated until the desired association of input/output node is learned.}
\label{fig2018} 
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


Here we discuss the implementation defining a simple input/output association task, similar to the one already discussed in the previous section for the case of the toy model of Fig.~\ref{fig1}: for each input node $i_n$, we ask the memristor network to learn a randomly chosen output node $o_m = M(i_n)$.

The proposed training involves the following sequence at each training step:

\begin{enumerate} 
\item \label{choosestep}Randomly choose an input node ($i_n$).
\item \label{readstep} Read the flowing through all the output nodes.
 Herein this is done by setting $i_n$ voltage to $V_\text{read}$, and moving the ammeter tip (which closes the circuit) to each output node sequentially ($o_1$, $o_2$, etc), while measuring the current.
\item Determine the output node with the maximum current.
\begin{enumerate}
 \item If the node with maximum current is the desired one, do nothing.
 \item \label{punishstep}Otherwise, that is if the output maximum current is not at the desired node, apply $V_\text{write}$, and go back to step 2.
\end{enumerate}
\item Go back to step 1.

\end{enumerate} 

$V_\text{read}$ is below $V_\updownarrow$ and its application does not change the resistances in the network.
$V_\text{write}$ is above $V_\updownarrow$ and with inverted polarity, hence it induces changes in the resistances of the network.
This is the only crucial factor, to ensure that the reading (in step \ref{readstep}) is not modifying the network conductances and conversely that the correction (in step \ref{punishstep}) decreases the likelihood of having large currents in the undesired paths.
It is evident that the memristor learning algorithm preserves the same spirit of the earlier work: to punish wrong paths by increasing the resistance of the involved memristors.
We applied step~\ref{punishstep} a maximum of $n_\text{max} = 80$ times.

To collect the statistics presented here, at the end of each cycle we read the currents for each input node ($i_1, i_2, \ldots$, considering each output node for each input).
We count how many of the input nodes point towards the desired output node, and define the error as the Hamming distance from the vector of largest outputs and the desired map.
The network has learned when the error is null, otherwise the cycle described above continues.
In some cases, after the network has learned, we will consider to train it with a different map, and so on.


\section{Results}
 
Now we proceed to describe the parametric behavior of the algorithm explained in the previous section.

First we explored the dependence of the learning time on the size of the middle layer $N_\text{bulk}$.
As discussed, larger values of $N_\text{bulk}$ in the neuron network model of Ref.~\cite{bak1,bak2} provided more paths to the correct output, which leads to shorter learning time.
We found very similar performance for the memristive network as shown in the results of Fig.~\ref{Fig4} for a three layer networks with $N_\text{in}=N_\text{out}=3$, and $N_\text{in}=N_\text{out}=4$, and several values of $N_\text{bulk}$.
In panels A and B, the fraction of networks that have learned the map as a function of the step number improves with larger $N_\text{bulk}$.
This is also apparent when we plot the fraction of networks that have successfully learned at (or before) correction step 1000 (see panels C and D) showing that performance is an increasing function of the middle layer size, $N_\text{bulk}$.

%%%%%%%%%%%%%%%%%%%FIG3 %%%%%%%%%%%%%%%%%%% 
\begin{figure} [ht!] 
\includegraphics [width=.9\textwidth]{Fig4New.pdf}
\caption{Learning performance as a function of the middle layer size.
Results show the success as the fraction of networks that successfully learn the input/output association map after a given number of correction steps.
Panel A: Success for $N_\text{in}=N_\text{out}=3$, $N_\text{bulk} = 20$, $100$ and $400$ as a function of the number of correction step.
Panel B: Same as panel A for $N_\text{in}=N_\text{out}=4$.
$N_\text{bulk} = 70$, $200$ and $600$.
Panel C: Success at 1000 steps, for $N_\text{in}=N_\text{out}=3$, as a function of several values of $N_\text{bulk}$.
Panel D: Same as panel C, for $N_\text{in}=N_\text{out}=4$.
In panels C and D the coloured symbols correspond to the results in panels A and B obtained with the respective $N_\text{bulk}$ values.
All results are averages over at least 500 network realisations, yielding values of standard errors smaller than the symbols size ($SEM \sim 0.025$).
Memristor parameters are detailed in the Appendix.
\label{Fig4}} 
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


The implementation of the training algorithm shows also that the memristors network learn a series of maps in a way similar to that exhibited by the earlier neuronal model~\cite{bak1,bak2}.
This can be seen in the example of Fig.~\ref{Fig5}, which shows the evolution of the network with $N_\text{in}=4$, $N_\text{bulk}=200$, $N_\text{out}=4$.
The network was trained in one of the labeled maps until eventually the error is zero, at this point it starts being trained on a different map and so on.
Notice the resemblance with the results in Fig.~\ref{fig1}, which suggests that the training strategy proposed here is capturing the essence of the learning algorithm of Ref.~\cite{bak1,bak2}.


%%%%%%%%%%%%%Fig4%%%%%%%%%%%%%%%%%%%%
\begin{figure} [ht!] 
\includegraphics [width=.9\textwidth]{Fig5New.pdf}
\caption{Typical evolution of the training of a network learning seven successive maps.
Upper main panel shows the error (Hamming distance from the desired to the current output) as a function of training steps.
The input/output maps, labeled \textbf{a} to \textbf{f}, are depicted in the right column and presented sequentially as indicated in the lower main panel.
Network with $N_\text{in}=N_\text{out}=4$, $N_\text{bulk} = 200$, other parameters same as in Fig.~\ref{Fig4}.
} \label{Fig5}
\end{figure}
 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Another distinctive property of the proposed training strategy is the fact that the memristive networks are robust to perturbations of the device properties.
This alterations can be seen, for instance, as changes in resistance, which in real networks can be due to volatility, defects, etc.
As an example, we plot in Fig.~\ref{Fig6} the evolution of a network ($N_\text{in}=N_\text{out}=4$, $N_\text{bulk} = 200$), which after learning the identity map, 
is periodically perturbed.
It can be seen that after each perturbation, the network recovers to null error learning the map in a few additional steps.
This ability is not surprising given the fact that the network proceeds with the perturbation in the same way than during the usual learning process.

%%%%%%%%%%%%% Figure 5%%%%%%%%%%%%%%
 \begin{figure} [ht!] 
\centerline{ \includegraphics [width = .5\linewidth] {Fig6New.pdf} }
\caption{Example of the network recovery after a single perturbation.
We plot the error (i.e., the Hamming distance from the desired to the current output) as a function of the training steps for a network which is learning the identity map.
100 steps after the map is learnt, $10\%$ randomly chosen memristors are perturbed by increasing their resistances by $5\%$.
 The first downward arrow indicates the first perturbation, while the second downward arrow denotes the 14th perturbation (which did not increase the network error).
Network with $N_\text{in}=N_\text{out}=4$, $N_\text{bulk} = 200$, other parameters same as in Fig.
4.}  \label{Fig6}
\end{figure}


To gain insight on the dynamics of the memristor network resistances during learning of successive maps a $N_\text{in}=N_\text{out}=3$, $N_\text{bulk}=400$ network was trained to learn all the possible $N_\text{out}^{N_\text{in}}=27$ maps.
After all maps were learned, the network location of each memristors were randomly shuffled (preserving their resistance values).
After that, the network was re-trained to learn the same $N_\text{out}^{N_\text{in}}$ maps.

In Fig.~\ref{Fig7}A, we show the histogram of resistances, at the beginning of the simulation, after learning all possible maps once (labeled 27) after shuffling and relearning all maps four times (labeled 108), and after shuffling and re-learning all maps ten times (labeled 270).
In panel B, we show the evolution of the average resistance, as a function of the number of learned maps.
As expected from the nature of the training algorithm, resistances can only grow when performing correction steps.
Moreover, from Fig.~\ref{Fig7}-A, the range of resistance values increases with the number of steps.
We remove the effect of growing resistance by normalising with the average resistance of the $i$th network, $R_i$.
In Panel C we plot a histogram of the normalised resistances, where each resistance value was divided by the average resistance of the network.
Finally, in Panel D, we show the coefficient of variation, $<CV>$, of a single network, computed as the standard deviation divided by the mean value of all resistances in the network.
After repeated learning the distribution tends to a Gaussian distribution, approaching a $<CV> \sim 1/3$.


 %%%%%%%%%%%%%%%%Figure 6%%%%%%%%%%%%%%%%
 \begin{figure} [ht!] 
\centerline{ \includegraphics [width = .95 \linewidth] {Fig7New.pdf}} 
\caption{Evolution of the distribution of resistance values after repeated re-learning of the same set of maps.
Panel A: Resistance histogram at the beginning of the simulation, after learning all possible maps once (labeled 27) after shuffling and relearning all maps four times (labeled 108) and after shuffling and re-learning all maps ten times (labeled 270).
The dashed vertical line shows the minimum value $R_{\text{min}}=50$.
Panel B: Average resistance as a function of number of learned maps.
Arrows indicate the maps used for the histograms in panel A.
Panel C: Same data as in panel A after normalising each memristor's resistance by the average value of the whole network.
Panel D: Coefficient of variation of memristor's resistance, as a function of the number of learned map.
 Results are calculated for a single network and then averaged over 25 network realisations.
Bin-size $\Delta R=5$ for histogram in Panel A and $\Delta R=0.05 R_i $ for panel B.
Network with $N_\text{in}=N_\text{out}=3$, $N_\text{bulk} = 400$, other parameters values as in Fig.
4.} 
 \label{Fig7}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



\section{Conclusions}

We have described a training procedure allowing a network of memristors to learn any arbitrary input/output association map.
This is achieved without any detailed information of the inner structure of the layers.
The approach requires only access to read and/or perturbe two nodes of the (arbitrarily large) networks at any given time.
Despite the extreme simplicity of the approach, it is demonstrated that an iterative reading of the current flowing between two points of the network, and its eventual perturbation, can modify the overall network connectivity to any desired configuration.
By design, the learning is robust against different types of perturbations, including differences and fluctuations in the memristors parameters, noise and defects as well as distribution of polarities (see Appendix).
Although the present results are limited to numerical simulations, in case of being implemented in hardware, the method is easily scalable to arbitrarily large network sizes.

Notice that the memristor network is able to learn despite lacking an important feature of its biological neural contra-part: the spikes.
That is not a limitation, because as commented in the introduction, in this kind of feedforward networks, the only role for the spikes would be to propagate the information from the input node/s trough the network to some output node/s.
Instead, in the present implementation, this is also achieved, but by the current flow from an external battery.
Thus, knowing which input node is connected the current flowing trough the network will be reflected on the value of the output current at a given node.
In this context, our approach is a simple solution which does not require at all of the implementation of any spiking mechanism.
Obviously, the absence of additional electronic to implement neurons became very relevant when considering a hardware implementation of this concept.
Concerning hardware, the proposed learning method benefits from variability in the network properties, therefore not requiring of precise control on the memristor parameter during manufacturing.

In these notes we limited ourselves to the presentation of the most fundamental aspects of the results.
As a salient feature, we reported how a single network can learn many maps, and this causes an expansion of the distribution of their resistances, possibly due to the multiplicity of solutions to learn a single map.
Nonetheless some few caveats must be mentioned\footnote{Exploration and further considerations regarding the memristive implementation can be read the blog \url{https://kakila.gitlab.io/mistake-learning/}}.
In the first place we consider adversarial situations for the algorithm, concerning some simple variations on the approach, where changes in the initial condition of memristor conductances, polarity as well as minor changes in the type of correction step are described. 
These studies are presented in the Appendix.
Second, we did not expanded here on discussing the type of problems that the present approach can solve.
This issue would require of extensive numerical simulations and it seems to deserve being explored on a hardware implementation, since it will work tens of orders of magnitude faster than any of our current numerical simulations.

In summary, we have introduced an algorithm able to train a memristor network to learn arbitrary associations.
Robust results for its performance were demonstrated using numerical simulations of a network of voltage controlled memristive devices.
Given the design principles, the results suggest that its implementation in hardware would be straightforward, being scalable and requiring very little peripheral computation overhead.

\newpage

\section{Appendix}
  
\emph{Memristor equations and parameters:}
The present results are obtained using Voltage Controlled Memristors with threshold, whose parameters are as follows: 
Each memristor has a resistance $R$, where $R_{\text{min}}\leq R \leq R_{\text{max}}$.
When a voltage $V$ is applied to it, a current $I$ passes through the memristor.
$R$ is modified by $V$ as long as it exceeds a given threshold $V_\updownarrow$.

The equations can be written as:

\begin{align} 
I &= V/R \\
\frac{\partial R}{\partial t} &= F(R,V) \label{MemriEQV}
\end{align}

where the function $F$ describe the behavior of the memristor.
For that we used the bipolar memristive system with threshold (BMS) of Ref.~\cite{MemEqs}, which is given by the equations:

\begin{equation}
F(R,V) = \begin{cases}
\beta (V + V_\updownarrow) & \text{if $V < -V_\updownarrow$, $R>R_{\text{min}}$ } \\
0 &\text{if $|V| <V_\updownarrow$} \\
\beta (V - V_\updownarrow) & \text{if $V > V_\updownarrow$, $R<R_{\text{max}}$ } 
\end{cases} \label{CasesV}
\end{equation}

and is illustrated in Fig.~\ref{Fig1Appx}.

%%%%%%%%%%%%%%%%%%%FIG F(R,V)%%%%%%%%%%% 
\begin{figure} [ht!] 
\includegraphics [width=.9\textwidth]{Fig1Appx.png}
\caption{Memristor behavior as a function of resistance and applied voltage.
The resistance of the memristor does not change unless the absolute value of the applied voltage is greater than $V_\updownarrow$.
Here we used $V_\updownarrow =0.075, R_\text{min}=75, R_\text{max}=5000, \beta=0.9$}
\label{Fig1Appx}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

For the numerical simulations we considered a feedforward network of memristors.
A relatively small (quenched) variability in the parameters of the device was used.
 The parameters for each memristor were randomly chosen from a uniform distribution with   $0.8<\beta<1$, $0.05<V_\updownarrow<0.1$, $50<R_{\text{min}}<100$, and $R_{\text{max}}=5000$.
Initial condition was set to $R=R_{\text{min}}$.
The reading step lasted $1$ time step using a voltage value $V_\text{read}=0.0001$.
$V_\text{write}$ polarity is opposite to $V_\text{read}$.
The correction step lasted 5 time steps using a voltage $V_\text{write}=0.2$.
Up to $n_{\text{max}}=80$ corrections attempts were performed.

Voltage values were chosen such that  $|V_\text{read}| \ll V_\updownarrow < V_\text{write}/2 $.
In this way the memristor properties does not change on the reading step, and only few memristors change its resistance during the correction step of the algorithm.

%%%%%%%%%%%%%%%%%%%FIG SM%%%%%%%%%%% 
\begin{figure} [ht!] 
\includegraphics [width=.9\textwidth]{FigSM.pdf}
\caption{Performance of the approach using random distribution of memristor polarities or random $V_\text{write}$ correction values.
Panel A: Success as a function of step number for networks where memristors polarities are chosen at random.
Panel B: Success as a function of step number for equal initial conditions $R_{\text{min}}=100$, and correction voltages $V_\text{write}$ uniformly drawn from $0.15$ to $0.3$.
Panel C: Initial and final resistance density distribution for the case of using random polarity.
Panel D: Initial and final resistance distribution histogram for the case of using random correction $V_\text{write}$.
For comparison, in Panels A and B the dashed line reproduces the results presented in Fig.~\ref{Fig4}B, black circles.
In Panels C and D, bin-width $\Delta R=5$.
Results are averaged over at least 100 network realisations, using in all cases, $N_\text{in}=N_\text{out}=4$, $N_\text{bulk}=200$, other parameters as described in the Appendix.
}
\label{SM}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\emph{Miscellaneous observations:} Some special cases are described here, including different initial conditions in the memristor parameters and variations on the implementation of the correction step, noting that all the results remain valid despite these changes.
 Fig.~\ref{SM}A shows the results of simulations where the polarity of the memristors were distributed randomly.
As a comparison we plotted the data presented already in Fig.~\ref{Fig4} (dashed line) corresponding to equal memristor polarity.
Panel C shows the changes in the $R$ distribution before and after the learning process.
 These results suggest that this factor produces only minor changes in the overall performance of the approach.


Then we explored the case of starting the simulation with identical resistances ($R=100$) for all memristors, and using random $V_\text{write}$ values in the correction steps (uniformly distributed from $0.15$ to $0.3$) while maintaining the other parameters $\beta$ and $V_\updownarrow$ randomly distributed.
Results are shown in Fig.~\ref{SM}.
It is apparent that the network learns approximately in the same manner as starting with random initial conditions for $R$, except that it takes additional steps to reach comparable success rates.
Probably these additional correction steps are trivially related to the time needed to generate a minimal dispersion on the $R$ values, needed for the approach to work.
In Fig.~\ref{SM}, panel C, the initial (i.e, $R=100$) and final distribution of resistances for this case are shown, showing the resulting broad $R$ distribution, after the map is learned.


\begin{thebibliography}{50}

\bibitem{Review1} Marković D, Mizrahi A, Querlioz A, Grollier J, (2020) Physics for neuromorphic computing.
{\it Nature Reviews Physics} {\bf 2}, 499--510.
doi:10.1038/s42254-020-0208-2
\bibitem{bak} Bak P.
(1996) How nature works: The science of self-organized criticality.
 Springer Science (New York).


\bibitem{bak1}Chialvo DR \& Bak P.
(1999) Learning from mistakes.
{\it Neuroscience} {\bf 90}, 1137.

\bibitem{bak2}Bak P \& Chialvo DR.
(2001) Adaptive learning by extremal dynamics and negative feedback.
{\it Phys.
Rev.
E} {\bf 63} 031 912.
\bibitem{joe1}Wakeling J.
(2003) Order--disorder transition in the Chialvo--Bak ``minibrain'' controlled by network geometry.
 {\it Physica A} {\bf 325}, 561--569.


\bibitem{joe2}Wakeling J.
(2004) Adaptivity and ``Per learning''.
{\it Physica A} {\bf 340}, 766--773.
 
 
\bibitem{brigman} Brigham M.
(2009) Self-Organised learning in the Chialvo-Bak model.
 MSc Dissertation.
Artificial Intelligence, School of Informatics.
University of Edinburgh.
UK.
 

\bibitem{chua}Chua LO.
(1971) Memristor--The missing circuit element.
{\it IEEE Transactions on Circuit Theory} {\bf 18}, 507--519 .
\bibitem{ReviewCarvelli} Caravelli, F \& Carbajal, JP (2018) Memristors for the curious outsiders {\it Technologies} {\bf 6}, (4) 118.
doi: 10.3390/technologies6040118

\bibitem{strukov} Strukov DB, Snider GS, Stewart DR, Williams RS, (2008) The missing memristor found {\it Nature} {\bf 453}, 80--83.
 
%\bibitem{chialvo} Chialvo DR.
(2010) Emergent complex neural dynamics.
{\it Nature Physics} {\bf 6}, 744.


\bibitem{MemEqs} Biolek D., Di Ventra M, Pershin YV.
(2013) Reliable SPICE Simulations of Memristors, Memcapacitors and Meminductors {\it Radioengineering}, {\bf 22}(4), 945--968.

\bibitem{MemEqs2}Querlioz D, Dollfus P, Bichler O, Gamrat C.
(2011) Learning with memristive devices: how should we model their behavior? IEEE/ACM International Symposium on Nanoscale Architectures (NANOARCH).
doi: 10.1109/NANOARCH.2011.5941497.

\end{thebibliography} 

\end{document} 
