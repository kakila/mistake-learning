Title: Winner-Takes-All dynamics
Date: 2020-06-17 23:17
Modified: 2020-08-29 04:18
Tags: idea, theory
Category: Concepts
Slug: wta-dynamics
Authors: JuanPi Carbajal
Summary: The long term behavior of winner-takes-all dynamics is investigated. Integer (instead of floating point number) dynamics are proposed.

Under *Winner-takes-all* dynamics the activation of the inputs nodes is propagated through the network by choosing, at each node, the children node with the highest weight.

In the diagram below we see a simple network. Each node is connected to a set of children nodes with a weighted edge. The activation will propagate over the outgoing edge with the highest weight (shown in black)

![wta-network with float weights]({static}/images/wta_network.gv.png)

The same network can be described with ranks instead of weights. Each node has a ranking of its children, and WTA propagates always using the first (rank 1) child.
The following diagram shows the network which now only uses integers.

![wta-network with integer weights]({static}/images/wta_network_integer.gv.png)

Under WTA these two networks are exactly the same.

Another characteristic of WTA propagation is that the path from input to output, for a fixed network structure (fixed rankings), is completely determined by the input node. Hence, any network, regardless of its depth, can me associated with a shallow network directly mapping inputs to outputs.

## Learning

(plots are generated using this [GNU Octave](www.octave.org) [script](https://gitlab.com/kakila/mistake-learning/-/blob/master/octave-mstklearn/s_wtasequence.m))

Under mistake learning, we reduce the weights of the edges if the association from input to output
is not correct (e.g. the activated output is not the desired for the given input).
The plot below show the time series of the weights in a node with 4 children, trying to learn an impossible task (i.e. the associations are always wrong).
The plots show the time series after 500 iterations that are used to "warm up" or "thermalize" the evolution.
The upper panel shows the actual weights (they are normalized at each step), and the middle panel shows the ID of the winner children.

After 500 iterations the dynamic is stationary (transients are not visible anymore), and each edge is consider the winner with almost the same frequency (bottom panel). Note however that for this punishment level (the weight step, here 0.261), the sequence of winner children is quite chaotic.

<img alt="float WTA time series" src="{static}/images/wta_TS.png" width="600" >

This behavior of the winner ID becomes very simple if the step is large enough, as illustrated in the figure below. The figure shows the number of iterations to get the winner child to be 1 again, as a function of the weight step. If the weight step is large, every time an edge is punished it goes down to the bottom of the ranking. Hence the stationary dynamic is just a rotation of the children (period of 4 in this case), which happens for steps near to 0.3 or larger. There is a region of maximum variation of these period. The weight step in the previous plot was chosen to be near this maximum.

<img alt="float WTA period" src="{static}/images/wta_Period.png" width="600" >

Hence for large weight steps, the punishment combined with WTA generates a rotation of the winner children. These behavior can be easily implemented in the integer network we showed before, hence removing the need to deal with floating point numbers.

The more chaotic behavior can also be implemented with random swapping of the ranks.
Provided that the task is not impossible, both methods are expected to converge.

These two options are implemented in [propagate_WTAint.m](https://gitlab.com/kakila/mistake-learning/-/blob/master/octave-mstklearn/propagate_WTAint.m).
