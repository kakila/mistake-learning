Title: A bipartite memristive Chialvo-Bak machine
Date: 2020-08-10 22:42
Modified: 2020-09-09 08:24
Tags: idea, theory
Category: Concepts
Slug: bipartite-MCBM
Authors: JuanPi Carbajal
Summary: difference between WTA dynamics in a Chialvo-Bak machine and the dynamics in memristive Chialvo-Bak machine

Here we continue discussing [A memristive Chialvo-Bak machine]({filename}/memchialvobak.md), please read that post
for an explanation of the teaching procedure. 

We need to define a symbol for convenience.
The reconfiguration of the machine to propagate the teaching signal can be described in two steps:

1. Set to open all inputs and outputs that were not active in the test.
    That is, all but the active input dictated by the task table and the observed active output are set to open.
    The active output differs from the (desired) output indicated in the task table, otherwise we wouldn't be teaching.

2. Set the input indicated by the task table to teaching mode.

We denote this process with $P(x, y)$, a short hand for "punish the connection between input $x$ and output $y$".

Consider the task

<style>
table{
    border-collapse: collapse;
    border-spacing: 0;
    width: 150px;
    margin-left: 0;
    margin-right: auto;
}
th{
    border-bottom:2px solid #000000;
}
td{
    border-bottom:1px solid #000000;
}
</style>
|**input** | **output** |
|----------|------------|
| $1$      | $4$        |
| $2$      | $3$        |

and the following network 

![bipartite network topology]({static}/images/memchialvobak_bipartite.svg)

Lets not consider the uninteresting situation in which by chance we got the right values for all resistances, and there 
is no need for learning. 
That is, we assume that the initial network does not solve the task. 
Hence, we will have to execute the teachings $P(1, 3)$ or $P(2, 4)$.

Without loss of generality, lets consider $P(1,3)$.
This punishment will induce currents through all the paths from output $3$ to input $1$.
This includes the path that needs to increase its resistance $(1, 3)$, but also includes 
the edges needed for the task, via the path $(1, 4, 2, 3)$.
The figure below illustrates the direction of the currents induced by $P(1,3)$.

![punishment of 1-3 connection]({static}/images/memchialvobak_bipartite_P13.svg)

We see that the connection $(1, 3)$ is punished as desired (resistance will increase due to teaching) but connection $(2, 3)$ is also punished, and connection $(2, 4)$, which is not desired, gets rewarded.

This example highlights the following issues

1. The WTA dynamics of Chialvo-Bak machines is not a good model for the propagation
   of signals in the memristive machine, because in the latter the signals flows through all available paths, not only the one with lowest resistance.
   
2. The proposed teaching method can fail with feedforward networks

The second issue seems to be not so bad, because it indicates that sometimes the method could work.
That's true, but the conditions for success are tantamount to having the problem solved from the beginning (might post analysis if I have time to do it, but it is straight forward in this network).
I do not know if this negative result holds for larger networks.
At first sight it seems to hold, because any feedforward network can be reduced to bipartite graph by the method of equivalence resistances.

The first issue also tell us that using only inputs and outputs for teaching will not be as easy
as in the classical machine.
It is difficult to only punish the path that lead to the error, using just inputs and outputs, without knowledge of the internal topology.

The current efforts are on:

1. Devising a new teaching method that uses only input and outputs.
2. Investigating what topologies do allow the current method to succeed

## Modified punishment

To avoid the undesired effects of the teaching procedure we make a slight modification of the punishment process.
The change is a reinterpretation of the first step, which now reads

1. Set to open all edges adjacent to the inputs and outputs that were not active in the test.

In the situation above, we now get

![new punishment of 1-3 connection]({static}/images/memchialvobak_bipartite_P13_v2.svg)

The current now is only through the undesired connection.
For networks with more paths connecting inputs with outputs the effect will not be so clear, but
this punishment does improve the previous version.

There are still initial conditions that are adverse. 
In particular, when the initial condition does not generate a solution to the task and the bad connections
have initial resistances close to the maximum.
In this situations punishment cannot increase the resistance of the bad connections enough to solve the task.
Luckily this problem is solved easily by "priming the network". 
This priming consist of propagating signals with mean values across the network to reduce the initial resistances.
 
Below is a summary of the simulation results with 200 randomly initialized networks

     (ChialvoBak): 
        Edges: 2 x 4 x 2
        Nodes: 2 x 0 x 2
        Frozen: True
        Data mode: 
           memnet.learningmachines.data_signal
           amplitude: 0.01
           samples: 100
        Teach mode: 
           memnet.learningmachines.teaching_signal
           amplitude: -0.1
           samples: 100
    
    Task:
    input	output
    0	0
    1	1

    Elapsed time 10.25115957899834 s
    Best error from 200 random networks: 0
    Successful attempts:  77.0% (154/200)
    Lucky initializations:  23.0% (46/200)
    Overall success:  100.0% (200/200)

    Final I/O:
    input	output
    0	0
    1	1

Which can be reproduced using the [plot_chialvobak_bipartite.py](https://kakila.gitlab.io/memnet/auto_examples/plot_chialvobak_bipartite.html#sphx-glr-auto-examples-plot-chialvobak-bipartite-py) example in the memnet library.

## Next

In a follow up post, we will be reproducing the results published in [@Bak2001].
