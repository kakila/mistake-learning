Title: Potential topics
Date: 2020-06-14 01:23
Modified: 2020-08-29 04:23
Tags: article, topics, planning
Category: Article content
Slug: article-topics
Authors: JuanPi Carbajal
Summary: A list and short descriptions of the topics that could be covered in the upcoming article


The following figure is useful to understand parts of this post. 
Have it in mind! 

<img alt="Encoding and Decoding mappings" src="{static}/images/EncodingDecoding.png" width="600" >


It is highly related to the posterior figure from [@Horsman2014] ([latex code](https://gitlab.com/kakila/memristors_review/-/blob/master/f_memr4dataproc.tex#L20))

<img alt="Computing with dynamical systems" src="{static}/images/Horsman2014_DScomputing.png" width="600" >

Some of the topics I will extend in its own posts.

## Single input activation

In the original version of the algorithm (*scalar*, or *sequential*, or *serial*) only one input node is active at a time.
As far as I can tell, this can be generalized.
Several input nodes can be active at the same time.
This allows for more flexible encodings and also for running inputs in parallel

## Encoding/Decoding 

The scalar algorithm permits encodings of the form

\begin{equation}
\mathcal{E}: V \rightarrow \text{In} \in \mathbb{N}, \quad \vert \text{In} \vert = N_\text{in} < \infty
\end{equation}

where $\text{In}$ is the set of input nodes. This means that we can represent only $N_\text{in}$ symbols in $V$. By allowing several inputs nodes to be active at the same time we can represent $N_\text{in}^2$.

## Network weights

The original algorithm used real numbers to encode weights. Since the propagation
of inputs to outputs is done via *Winner-Takes-All* (WTA) dynamics, real numbers are not necessary.
The whole algorithm can be represented with integer numbers (actually, naturals should be enough).
I explore this possibility in the [Winner-Takes-All dynamics]({filename}/wtadynamics.md) post.

## Topology

The network is given by the tuple $\left(G, P, L\right)$, where $P$ and $L$ are the propagation and the learning rules (or dynamics); and $G$ is a graph, i.e. a tuple $(N, E)$ of nodes and edges.

The valid network topologies are constrained by the propagation rule.
Currently WTA is used, then we get the following constraint

* The out-degree of all nodes should be 2 or greater

Are there more constraints? e.g. Can we have in-degree equal to 1?

I split the set of nodes $N$ in the network in three set based on their relation to the input and output nodes, $\text{In}$ and $\text{Out}$, respectively:

\begin{align}
\text{Interface}_\text{in} &= \lbrace n \in N : \operatorname{parents}(n) \cap \text{In} \neq \emptyset \rbrace\\
\text{Interface}_\text{out} &= \lbrace n \in N : \operatorname{children}(n) \cap \text{Out} \neq \emptyset \rbrace\\
\text{Bulk} &= \lbrace n \in N : \operatorname{neighbors}(n) \cap \left( \text{In} \cup \text{Out} \right) = \emptyset \rbrace
\end{align}

### Cycles

As far as I can test, the algorithm does not work with networks with cycles. I think this can be fixed with an extra rule: 

* If during propagation the active node comes back to the initial node, the path should also be depressed (punishment of cycles). 

However is an ad-hoc solution. Maybe is worth investing time in thinking how nature can be exploiting cycles.

## What triggers learning?

I think there are two aspects that is worth differentiating. On the one hand we have the way we adapt the weights, either by reinforcement or by depression. On the other hand we have the type of event that triggers the adaptation, either success or failure. I think the core concept is the second, and it is what makes the algorithm stand out.

## Memristive implementation

* Polarity distribution
* What is the effect of volatility?

## Demo tasks

- $\mathbb{N}^d$ assignations, e.g. all operations modulo $x$.
- Logical (reversible) operators
- Autoencoder

## Adaptation

The hypothesis is that reinforcement learning generates highly stable networks, while mistake-learning keeps the network in a state of high susceptibility.
Quantify this by comparing the time to adapt to a sudden change in the task.


