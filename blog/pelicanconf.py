#!/usr/bin/env python

AUTHOR = 'JuanPi Carbajal'
SITENAME = 'Learning from Mistakes'
SITEURL = ''

PATH = 'content'
OUTPUT_PATH = 'public'

TIMEZONE = 'Europe/Zurich'

DEFAULT_LANG = 'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (
        ('Repository', 'https://gitlab.com/kakila/mistake-learning/'),
        )

# Social widget
SOCIAL = (
          ('Twitter', 'https://twitter.com/JuanPiCarbajal'),
         )

DEFAULT_PAGINATION = 10

# Plugins
# Path to Plugins
PLUGIN_PATHS = ['plugins']

from pelican.plugins import simple_footnotes as footnotes
PLUGINS = [
  "render_math",
  "pelican-cite",
  footnotes
  ]

PUBLICATIONS_SRC = 'content/references.bib'

# plugins and cache may interfere
LOAD_CONTENT_CACHE = False
