# Copyright (C) 2020 Juan Pablo Carbajal
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

Nin = 20;
Nout = Nin;
N = Nin + Nout;
innodes = (1:Nin).';
outnodes = (Nin+1:N).';

Ntests = 1000;
Task = (1:Nin).';
duration = zeros (Ntests, 1);
hbar = waitbar(0, 'Evaluating taks');
for k=1:Ntests
  waitbar (k/Ntests, hbar);
  #Task(:,2) = randperm (Nin);
  Task(:,2) = outnodes(randi (Nout, size (Task, 1), 1));

  # Random network
  # Children ranking, for each node (row) describe the ranking of children (columns)
  # WTA takes the first
  ChildrenRanking_abs = zeros (N);
  for i=1:N
    ChildrenRanking_abs(i,:) = randperm (N);
  endfor

  # WTA propagate inputs
  out = propagate_WTAint(ChildrenRanking_abs, Task(:,1), outnodes);
  mistake = (out != Task(:,2));

  # WTA updates
  iter = 0;
  while any(mistake)
    [ChildrenRanking_abs out mistake niter] = ...
                                  train_WTAint(ChildrenRanking_abs, Task, 
                                                outnodes, N, 'rotate');
    iter += niter;
  endwhile
  duration(k) = iter;
endfor
delete (hbar)

figure (1)
clf
hist (duration)
xlabel ('iterations until no error')
ylabel ('frequency')

