# Copyright (C) 2020 Juan Pablo Carbajal
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

function path = propagate_WTAint(net, inputs, outputs, return_path=false)
  wta = net(:,1);
  # nodes marked as output are they own children
  wta(outputs) = outputs;
  # nodes without children are they own children
  nochild = find(!isnodeid (wta));
  wta(nochild) = nochild;

  path = inputs(:);
  hasnext = !ismember (path, outputs);
  visited = path;
  while any(hasnext)
    current = wta(path(:, end));
    if return_path
      path(:, end+1) = current;
    else
      path = current;
    endif

    isout   = ismember (current, outputs);
    iscycle = ismember (current, visited);
    hasnext = !isout & !iscycle;
    visited = union (visited, current);

    # if we are in a cylce we stay in current node
    wta(iscycle) = current(iscycle);
  endwhile

endfunction
